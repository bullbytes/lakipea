# Lakipea
An experimental web server built on sockets.

Start it with `stack ghci` from the project's root and then run `main`.

The file `config/routes.yaml` determines where Lakipea looks for files. Put your HTML, CSS, and whatnot into `web_files/me`, for example. Then point `config/routes.yaml` to it:

    content_to_hosts:
      web_files/me:
      - localhost
      - realdomain.me

The server comes with self-signed TLS certificates for testing. Send the server an HTTPS request with

    curl -k https://localhost:8443

Check out `config/lakipea_runtime.yaml` to change ports and the location of TLS certificates.
