{-# Language QuasiQuotes, FlexibleContexts, OverloadedStrings,
             PatternSynonyms, ScopedTypeVariables #-}

-- This module starts an HTTPS and an HTTP server.
-- See the README file for more information.
module Main where
import           System.Environment             ( getArgs )
import           GHC.Conc                       ( numCapabilities )

import           NeatInterpolation


import           LakipeaTypes

import qualified Data.Text                     as T
import           System.IO

import qualified TcpServer                      ( run )
import qualified Http.Server                   as HttpServer
                                                ( handleConnection )
import qualified Http.TlsServer                as HttpsServer
                                                ( handleConnection
                                                , loadCredentials
                                                )

import           Colog                          ( pattern I
                                                , pattern D
                                                , pattern W
                                                , Message
                                                , LogAction(..)
                                                , log
                                                )

import           Control.Monad.Reader           ( ask )

import           System.FilePath
import           Control.Monad.IO.Class         ( MonadIO(..) )

import qualified Utils.LogUtil                 as LogUtil
                                                ( withFile
                                                , modifyLogAction
                                                )
import           Utils.AsyncUtil

import           Prelude                 hiding ( log )
import           ConfigReader

main :: IO ()
main = do
  config <- readConfig $ configDir </> "lakipea_runtime.yaml"
  let logFile = "lakipea.log"
  LogUtil.withFile logFile $ \textFileLogAction -> do
    let lakipeaLogging = LogUtil.modifyLogAction textFileLogAction
    errorOrRoutes <- getRoutes $ configDir </> "routes.yaml"
    case errorOrRoutes of
      (Left error) ->
        putStrLn ("Couldn't parse routing file. Error is: " ++ error)
      (Right routes) -> do
        let env = lakipeaEnv config lakipeaLogging routes
        putStrLn $ "Application starting, logging to " ++ logFile
        runApp env startServers
  where configDir = "config"

lakipeaEnv :: Config -> LogAction App Message -> Routes -> Env App
lakipeaEnv config logAction routes =
  Env { config = config, logAction = logAction, routes = routes }

-- Starts the HTTPS and the HTTP server.
startServers :: App ()
startServers = do
  log I "Starting Lakipea HTTPS and HTTP servers"
  env <- ask
  let conf = config env
  log I $ T.pack $ show conf
  logRuntimeInfo
  let pubCert = tlsPublicCertificate conf
  let privKey = tlsPrivateKey conf
  msgOrCreds <- liftIO $ HttpsServer.loadCredentials pubCert privKey
  case msgOrCreds of
    (Left msg) ->
      let msgText = T.pack msg
      in  log W [text|Could not load TLS credentials. Error: $msgText|]
    -- raceWith_ starts two green threads running the HTTP and the HTTPS servers. Function cancelOnSigTerm makes sure the threads get an exception thrown at them when the main process receives a SIGTERM. Using this exception, the underlying TCP server can clean up, for example by closing sockets
    (Right tlsCreds) -> liftIO $ raceWith_
      cancelOnSigTerm
      (runApp env $ startHttps conf tlsCreds)
      (runApp env $ startHttp conf)
  liftIO $ putStrLn "Could not start servers. Check log"
 where
  startHttps conf tlsCreds =
    TcpServer.run (httpsPort conf) (HttpsServer.handleConnection tlsCreds)
  startHttp conf = TcpServer.run (httpPort conf) HttpServer.handleConnection
  logRuntimeInfo = do
    cmdLineArgs <- liftIO getArgs
    let cmdLineArgsLength = T.pack $ show $ length cmdLineArgs
    let cmdLineArgsText   = T.pack $ unwords cmdLineArgs
    log D [text|$cmdLineArgsLength command line arguments: $cmdLineArgsText|]
    let nrOfCoresText = T.pack $ show numCapabilities
    log D [text|Number of cores: $nrOfCoresText|]
