{-# Language OverloadedStrings #-}
module ConfigReader
  ( readConfig
  , getRoutes
  )
where
import           LakipeaTypes
import           Data.Yaml

import           Control.Applicative            ( (<$>) )

import qualified Data.Text                     as T
                                                ( pack )
import           Data.Map                       ( Map
                                                , toList
                                                , insert
                                                , mapKeys
                                                )
import qualified Data.Map                      as Map
                                                ( empty )
readConfig :: FilePath -> IO Config
readConfig = decodeFileThrow

getRoutes :: FilePath -> IO (Either ErrorMsg Routes)
getRoutes configFile = do
  exOrParsedConf <- decodeFileEither configFile
  return $ either (Left . prettyPrintParseException)
                  (Right . toRoutes)
                  exOrParsedConf

toRoutes :: Map FilePath HostsAndRedirects -> Routes
toRoutes configMap = foldr
  (\(contentDir, hostsAndRedirects) routes ->
    let hostnames = hosts hostsAndRedirects
    -- Add each hostname and its content dir props to the routes
    in  foldr
          (\host tempRoutes -> insert
            host
            (ContentProps contentDir (httpsRedirects hostsAndRedirects))
            tempRoutes
          )
          routes
          hostnames
  )
  Map.empty
  (toList configMap)


data HostsAndRedirects = HostsAndRedirects { hosts :: [HostName], httpsRedirects :: HttpsRedirects} deriving Show

instance FromJSON HostsAndRedirects where
  parseJSON (Object v) = HostsAndRedirects <$> v .: "hosts" <*> fmap
    toRedirects
    (v .: "https_redirects")
   where
    toRedirects :: Map String String -> HttpsRedirects
    toRedirects map =
      mapKeys (Resource . T.pack) $ fmap (Resource . T.pack) map

  parseJSON _ = fail "Expected Object for HostsAndRedirects value"
