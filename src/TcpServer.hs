{-# Language QuasiQuotes, OverloadedStrings, PatternSynonyms #-}
module TcpServer
  ( run
  )
where
-- https://hackage.haskell.org/package/network-2.8.0.1/docs/Network-Socket.html
-- See Michael Kerrisk's "The Linux Programming Interface" for more on sockets.
import           Network.Socket          hiding ( recv )
import           Control.Monad.IO.Class         ( liftIO )
import qualified Data.Text                     as T
                                                ( pack )
import           Utils.LogUtil                  ( logException )

import           Prelude                 hiding ( log )

import           Colog                          ( log
                                                , pattern I
                                                , pattern D
                                                , Severity(Warning)
                                                )
import           NeatInterpolation
import           Control.Exception              ( SomeException )

import qualified UnliftIO.Concurrent           as UC
                                                ( forkFinally )

-- https://hackage.haskell.org/package/unliftio-0.2.12/docs/UnliftIO-Exception.html#v:bracket
import qualified UnliftIO.Exception            as UE
                                                ( bracket )

import           LakipeaTypes

run :: Int -> (Socket -> App ()) -> App ()
run portNr handleConnection = do
  let portNrText = T.pack $ show portNr
  log I [text|Start handling connections on port $portNrText|]
  UE.bracket
    -- Acquire the listening socket
    (do
      (serverSock, addrInfo) <- liftIO $ getListeningSocket portNr
      let addrInfoText = T.pack $ show addrInfo
      log I [text|Created listening socket at: $addrInfoText|]
      return serverSock
    )
    -- If the server loop is interrupted (with SIGTERM, for example), we close the socket
    (\serverSock -> do
      liftIO $ close serverSock
      log I [text|Closed listening socket on port $portNrText|]
    )
    -- The server loop: accept connections on the listening socket and handle connections
    loop
 where
  loop :: Socket -> App ()
  loop serverSock = do
    log D "Waiting for client connection"
    (connSock, clientAddress) <- liftIO $ accept serverSock
    let clientAddressText = T.pack $ show clientAddress
    log I [text|Accepted connection with $clientAddressText|]
    -- Forks a thread to handle the client connection. When the
    -- thread is about to terminate, close the socket
    _threadId <- UC.forkFinally (handleConnection connSock)
                                (closeSockAndLog connSock clientAddress)
    loop serverSock

  closeSockAndLog :: Socket -> SockAddr -> Either SomeException () -> App ()
  closeSockAndLog connSock clientAddress excOrVoid = do
    liftIO $ close connSock
    let clientAddressText = T.pack $ show clientAddress
    log I [text|Closed connection with $clientAddressText|]
    -- Log the exception if there is one, do nothing otherwise
    either (logException Warning) return excOrVoid

getListeningSocket :: Int -> IO (Socket, AddrInfo)
getListeningSocket portNr = do
  addrInfo <- getSocketAddress portNr
  sock     <- socket (addrFamily addrInfo)
                     (addrSocketType addrInfo)
                     (addrProtocol addrInfo)
  configure sock
  bind sock (addrAddress addrInfo)
  listen sock maxNrOfQueuedConnections
  return (sock, addrInfo)
 where
  maxNrOfQueuedConnections = 5
  configure :: Socket -> IO ()
  configure socket = do
    -- https://stackoverflow.com/questions/6125068/what-does-the-fd-cloexec-fcntl-flag-do
    setCloseOnExecIfNeeded $ fdSocket socket

    -- Setting the SO_REUSEADDR option means that we can bind a socket to a local port
    -- even if another TCP endpoint is bound to the same port either because:
    -- 1. A previous invocation of the server that was connected to a client
    -- performed an active close, either by calling close(), or by crashing (e.g., it was
    -- killed by a signal). This leaves a TCP endpoint that remains in the TIME_WAIT
    -- state until the 2MSL timeout expires.
    --
    -- 2. A previous invocation of the server created a child process to handle a connection
    -- to a client. Later, the server terminated, while the child continues to serve
    -- the client, and thus maintain a TCP endpoint using the server's well-known port.
    setSocketOption socket ReuseAddr 1

  getSocketAddress :: Int -> IO AddrInfo
      -- getAddrInfo resolves a hostname to one or more addresses (possibly using DNS) returning the best first.
      -- It's multiple addresses if, for example, the host has more than one network interface.
      -- It also contains a socket address created using the hints.
      -- See also https://hackage.haskell.org/package/network-3.1.1.0/docs/Network-Socket.html#v:getAddrInfo
  getSocketAddress portNr = head <$> getAddrInfo hints hostAddress serviceName
   where
    hostAddress = Nothing -- ^Nothing gets the wildcard address. For IPv6 this is 0::0 and for IPv4 it's 0.0.0.0
    serviceName = Just (show portNr) -- ^Can also be a string like "http"
    hints       = Just $ defaultHints { addrFlags      = [AI_PASSIVE] -- ^A passive socket listens for incoming client connections. AI stands for "address info".
                                      , addrSocketType = Stream     -- ^Stream means we want a TCP connection.
                                      , addrFamily     = AF_INET6   -- ^AF_INET6 is IPv6. AF stands for "address family".
                                      }
