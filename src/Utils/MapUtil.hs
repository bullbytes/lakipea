
module Utils.MapUtil where

import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
                                                ( toList
                                                , fromList
                                                )

-- Inverts a map. For example, turns [("A", [1,2]), ("B", [3])] into
-- [(1 -> "A"), (2 -> "A"), (3 -> "B")]
-- If a value occurs multiple times, the last occurence is used:
-- [("A", [1, 2, 3]), ("B", [3])] becomes [(1,"A"),(2,"A"),(3,"B")]
invert :: (Ord k, Ord v) => Map k [v] -> Map v k
invert map = Map.fromList pairs
  where pairs = [ (v, k) | (k, vs) <- Map.toList map, v <- vs ]
