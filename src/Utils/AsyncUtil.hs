module Utils.AsyncUtil where

import           Control.Concurrent.Async       ( withAsync
                                                , cancel
                                                , Async
                                                , waitEither_
                                                )
import           System.Posix.Signals

-- raceWith_ :: (Async a -> IO b) -> IO a -> IO a -> IO ()
raceWith_ f left right = withAsync left $ \a -> withAsync right $ \b -> do
  f a
  f b
  waitEither_ a b

cancelOnSigTerm :: Async a -> IO Handler
cancelOnSigTerm asy = installHandler
  sigTERM
  (Catch $ do
    putStrLn "Caught SIGTERM"
    -- Throws an AsyncCancelled exception to the forked thread, allowing
    -- it to release resources via bracket
    cancel asy
  )
  Nothing
