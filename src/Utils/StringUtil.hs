module Utils.StringUtil
  ( strip
  , stripLeading
  , stripTrailing
  , toLowerCase
  )
where
import           Data.List                      ( dropWhileEnd )
import           Data.Char                      ( toLower )

stripLeading :: Char -> String -> String
stripLeading charToStrip = dropWhile (== charToStrip)

stripTrailing :: Char -> String -> String
stripTrailing charToStrip = dropWhileEnd (== charToStrip)

strip :: Char -> String -> String
strip charToStrip = stripLeading charToStrip . stripTrailing charToStrip

toLowerCase :: String -> String
toLowerCase = map toLower
