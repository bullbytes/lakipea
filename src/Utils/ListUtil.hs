module Utils.ListUtil  where

import           Data.List                      ( elemIndex )
import           Safe                           ( atMay )

getElemAfter :: [String] -> String -> Maybe String
getElemAfter list elem = do
  idx <- elemIndex elem list
  atMay list (1 + idx)

splitAtFirst :: Eq a => a -> [a] -> ([a],[a])
-- fmap applies drop 1 to the second element of the pair, removing
-- the element that we split on
splitAtFirst x = fmap (drop 1) . break (x ==)
