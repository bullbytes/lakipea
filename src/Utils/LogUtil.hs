{-# Language RankNTypes, FlexibleContexts   #-}
module Utils.LogUtil where

import           Control.Exception              ( Exception
                                                , displayException
                                                )
import           GHC.Stack                      ( withFrozenCallStack )
import qualified Data.Text                     as T

import           Colog                          ( log
                                                , Message
                                                , WithLog
                                                , Severity
                                                , LogAction(..)
                                                , cmapM
                                                , upgradeMessageAction
                                                , log
                                                , fmtRichMessageDefault
                                                , defaultFieldMap
                                                )
import qualified Data.Text.IO                  as TIO

import           System.IO                      ( hFlush
                                                , IOMode(AppendMode)
                                                , withFile
                                                )
import           Control.Monad.IO.Class         ( MonadIO(..) )
import           Prelude                 hiding ( log )


logException
  :: forall e m env
   . (WithLog env Message m, Exception e)
  => Severity
  -> e
  -> m ()
logException severity =
  withFrozenCallStack (log severity . T.pack . displayException)


modifyLogAction logAction =
  let combinedLogAction = {- logTextStdout <> -}logAction
  -- https://hackage.haskell.org/package/co-log-0.3.0.0/docs/Colog-Message.html#v:fmtRichMessageDefault
      messageAction = cmapM fmtRichMessageDefault combinedLogAction
  in  upgradeMessageAction defaultFieldMap messageAction

-- Logs to a file eagerly (not buffering the messages)
-- From here:
-- https://github.com/kowainik/co-log/issues/176#issuecomment-578730791-permalink
withFile
  :: forall m r . MonadIO m => FilePath -> (LogAction m T.Text -> IO r) -> IO r
withFile path action =
  System.IO.withFile path AppendMode $ action . logTextFlushHandle
 where
  logTextFlushHandle handle = LogAction $ \msg -> liftIO $ do
    TIO.hPutStrLn handle msg
    hFlush handle
