{-# Language PatternSynonyms, FlexibleContexts, ScopedTypeVariables,
   GeneralizedNewtypeDeriving, OverloadedStrings #-}

-- We need these three to create the instance of HasLog
{-# Language InstanceSigs, MultiParamTypeClasses, FlexibleInstances #-}
module LakipeaTypes where

import           Colog                          ( Message
                                                , LogAction
                                                , HasLog(..)
                                                )
import           Control.Monad.IO.Class         ( MonadIO )
import           Control.Monad.Reader           ( MonadReader
                                                , ReaderT(..)
                                                , ask
                                                , local
                                                )
import qualified Data.Yaml                     as Y
import           Data.Yaml                      ( FromJSON(..)
                                                , (.:)
                                                )
import           Data.Map                       ( Map )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
                                                ( unpack )
import           Control.Monad.IO.Unlift

-- The host that the client is trying to reach, does not contain the port number. Examples: mb.sb, bullbytes.com, bullbyt.es
newtype HostName = HostName String deriving (Eq, Ord, FromJSON)

type HostsToContentDir = Map HostName FilePath
type ErrorMsg = String

type Source = Resource
type Destination = Resource

type HttpsRedirects = Map Source Destination

data ContentProps = ContentProps {contentDir :: FilePath, redirects:: HttpsRedirects} deriving (Show, Eq, Ord)


type Routes = Map HostName ContentProps

instance Show HostName where
  show (HostName hostNameStr) = hostNameStr

-- A resource the client has requested via HTTP
newtype Resource = Resource Text deriving (Eq, Ord)

instance Show Resource where
  show (Resource hostNameTxt) = T.unpack hostNameTxt

-- The environment for Lakipea, m will be our App → the log action
-- will take place inside our app
data Env m = Env
    { config :: !Config
    , logAction  :: !(LogAction m Message)
    , routes :: !Routes
    }

data Config = Config {
    httpsPort :: !Int
  , httpPort :: !Int
  , httpsRedirectPort :: !Int
  , tlsPrivateKey :: !FilePath
  , tlsPublicCertificate :: !FilePath
  , acmeChallengeWebroot :: !FilePath
  , lakipeaVersion :: !Text

  } deriving (Eq, Show)

instance FromJSON Config where
  parseJSON (Y.Object v) =
    Config
      <$> v
      .:  "https-port"
      <*> v
      .:  "http-port"
      <*> v
      .:  "https-redirect-port"
      <*> v
      .:  "tls-private-key"
      <*> v
      .:  "tls-public-certificate"
      <*> v
      .:  "acme-challenge-webroot"
      <*> v
      .:  "lakipea-version"

  parseJSON _ = fail "Expected Object for Config value"


instance HasLog (Env m) Message m where
  getLogAction :: Env m -> LogAction m Message
  getLogAction = logAction
  {-# INLINE getLogAction #-}

  setLogAction :: LogAction m Message -> Env m -> Env m
  setLogAction newLogAction env = env { logAction = newLogAction }
  {-# INLINE setLogAction #-}

newtype App a = App
    {
    -- Env is the function argument we provide implicitly. Env has a type
    -- parameter, our app, so we know in which monad to log.
    -- IO is the monad that our app runs in
    unApp :: ReaderT (Env App) IO a
    } deriving MonadUnliftIO --  deriving (Functor, Applicative, Monad, MonadIO, MonadReader (Env App))

instance Functor App where

  fmap aToB (App (ReaderT envToIOa)) =
    App $ ReaderT $ \env -> let ioA = envToIOa env in fmap aToB ioA

instance Applicative App where

  pure x = App $ ReaderT $ \_ -> return x

  (App (ReaderT envToIOaToB)) <*> (App (ReaderT envToIOa)) =
    App $ ReaderT $ \env ->
      let ioToAb = envToIOaToB env
          ioA    = envToIOa env
      in  ioToAb <*> ioA

instance Monad App where
  (App (ReaderT envToIOa)) >>= aToApp = App $ ReaderT $ \env ->
    let ioA = envToIOa env
    in  ioA >>= (\a -> let (App (ReaderT envToIOb)) = aToApp a in envToIOb env)

instance (MonadReader (Env App)) App where

  ask = App $ ReaderT $ \env -> return env
  local changeEnv (App (ReaderT envToIOa)) =
    App $ ReaderT $ \env -> envToIOa (changeEnv env)

instance MonadIO App where
  liftIO ioA = App $ ReaderT $ \_ -> ioA

runApp :: Env App -> App a -> IO a
runApp env app =
  -- We're applying the environment to the application, producing IO
                 runReaderT readerT env
  where readerT = unApp app
