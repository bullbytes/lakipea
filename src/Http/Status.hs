module Http.Status where

data HttpStatus = HttpStatus Int String

-- See https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
ok = HttpStatus 200 "OK"
movedPermanently = HttpStatus 301 "Moved Permanently"
badRequest = HttpStatus 400 "Bad Request"
forbidden = HttpStatus 403 "Forbidden"
notFound = HttpStatus 404 "Not Found"

instance Show HttpStatus where
  show (HttpStatus code message) = show code <> " " <> message
