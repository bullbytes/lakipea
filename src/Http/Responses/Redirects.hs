{-# Language OverloadedStrings #-}
module Http.Responses.Redirects where

import           Data.ByteString.Lazy.UTF8     as BSU
                                                ( fromString )
import           Data.ByteString.Lazy           ( ByteString )
import           LakipeaTypes
import           Http.Responses.Util            ( mkHeader )
import           Http.Constants                 ( lineBreak )
import           Http.Status                   as Status
import           Data.Text                      ( isPrefixOf )

redirectResponse :: HostName -> Int -> Resource -> ByteString
-- We redirect the client to HTTPS using a header like this:
-- HTTP/1.1 301 Moved Permanently
-- Location: https://example.org:443/theResource
redirectResponse host redirectPort resource =
  let headerLines =
          [ "Location: https://"
              ++ show host
              ++ ":"
              ++ show redirectPort
              -- Make sure a slash separates port and resource
              ++ show (resourceWithSlash resource)
          ]
      -- There has to be a line break after the header lines
      response = mkHeader Status.movedPermanently headerLines <> lineBreak
  in  BSU.fromString response
 where
  resourceWithSlash :: Resource -> Resource
  resourceWithSlash r@(Resource resource) = if "/" `isPrefixOf` resource
    then r
    else Resource $ "/" <> resource
