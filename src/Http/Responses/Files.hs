{-# Language QuasiQuotes, OverloadedStrings, PatternSynonyms #-}
module Http.Responses.Files
  ( fileResponse
  , acmeResponse
  )
where
import           NeatInterpolation
import           System.Directory               ( getCurrentDirectory )
import           System.FilePath
import           Control.Exception
import           Utils.StringUtil               ( stripLeading )

import           Data.List                      ( isPrefixOf )

import           Http.ContentType               ( getContentType
                                                , html
                                                , plainText
                                                )
import           Network.Mime
import qualified Data.Text.Lazy                as LT
                                                ( pack )
import qualified Data.Text                     as T
                                                ( unpack
                                                , pack
                                                )
import           Data.Text.Lazy.Encoding        ( encodeUtf8 )
import qualified Data.ByteString.Lazy          as LB

import qualified Data.Map                      as Map
                                                ( lookup )

import           Control.Monad.IO.Class         ( liftIO )

import           LakipeaTypes

import           Prelude                 hiding ( log )
import           Colog                          ( log
                                                , pattern W
                                                , pattern D
                                                , Severity(Warning)
                                                )

import           Utils.LogUtil                  ( logException )
import           Http.Status
import           Control.Monad.Reader           ( ask )
import           Http.Responses.Redirects       ( redirectResponse )
import           Http.Responses.Errors         as ErrorResponses
import           Http.Responses.Util            ( toResponse_ )
import           Data.Maybe                     ( maybe )

-- Responds with the content of the requested resource
-- If the resource is Nothing, responds with index.html
fileResponse :: Resource -> HostName -> App LB.ByteString
fileResponse requestedResource hostName = do
  env <- ask

  let hostNameText = T.pack (show hostName)
  log D [text| Host name is: $hostNameText|]
  let contentPropsMb = Map.lookup hostName $ routes env
  let contentDirMb   = fmap contentDir contentPropsMb
  let contentDirText = T.pack (show contentDirMb)
  log D [text| Content dir is: $contentDirText|]

  maybe noContentForHost
        (fileResponseOrRedirect requestedResource hostName)
        contentPropsMb
 where
  noContentForHost = return
    (ErrorResponses.badRequest
      (LT.pack
        ("This server has no content for the requested host: " ++ show hostName)
      )
    )

fileResponseOrRedirect
  :: Resource -> HostName -> ContentProps -> App LB.ByteString
fileResponseOrRedirect requestedResource hostName contentProps =
  let redirectLocationMb = getRedirectLocation requestedResource contentProps
  -- There's no redirect defined for this resource → Serve the file
  in  case redirectLocationMb of
        Nothing -> do
          log D $ T.pack $ "No redirect for resource " <> show requestedResource
          (responseBody, contentType, status) <- getResource
            requestedResource
            (contentDir contentProps)
          return $ toResponse_ responseBody contentType status

        Just redirectLocation -> do
          env <- ask

          log D $ T.pack $ "Redirect for resource " <> show requestedResource <> " to " <> show redirectLocation
          return $ redirectResponse hostName
                                    (httpsRedirectPort $ config env)
                                    redirectLocation

getRedirectLocation :: Resource -> ContentProps -> Maybe Resource
getRedirectLocation resource contentProps =
  Map.lookup resource $ redirects contentProps

getResource :: Resource -> FilePath -> App (LB.ByteString, MimeType, HttpStatus)
getResource resource contentDir = do
  dir <- liftIO getCurrentDirectory
  let webFilesDir       = dir </> contentDir
  let requestedFile     = getFileToServe resource webFilesDir
  -- Make sure only files inside the web files dir are served
  let requestedFileText = T.pack requestedFile
  if isInside webFilesDir requestedFile
    then do
      log D [text| File to serve: $requestedFileText|]
      readFileOrGetNotFoundHtml requestedFile
    else do
      log
        W
        [text| Requested file is outside web files directory: $requestedFileText|]
      getForbiddenText requestedFile

 where
  getForbiddenText :: FilePath -> App (LB.ByteString, MimeType, HttpStatus)
  getForbiddenText fileToServe = return
    (ErrorResponses.forbidden fileToServe, plainText, Http.Status.forbidden)

-- Get the file path on the server of the resource that
-- the client requested
getFileToServe :: Resource -> FilePath -> FilePath
getFileToServe (Resource reqResourceTxt) webFilesDir =
  -- We remove the slash since pathname's </> combines
  -- "dir" </> "/file" to just "/file" and not "dir/file"
   webFilesDir </> stripLeading '/' (T.unpack reqResourceTxt)

acmeResponse :: Resource -> FilePath -> App LB.ByteString
acmeResponse reqResource dir = do
          let fileToServe = getFileToServe reqResource dir
          (responseBody, contentType, status) <- readFileOrGetNotFoundHtml fileToServe
          return $ toResponse_ responseBody contentType status

-- Checks whether a file is inside a directory or its subdirectories
isInside :: FilePath -> FilePath -> Bool
-- We add a trailing slash to the dir so "/home/me/dir" isn't interpreted
-- as containing "/home/me/dir2"
isInside dir file = addTrailingPathSeparator dir `isPrefixOf` file

readFileOrGetNotFoundHtml
  :: FilePath -> App (LB.ByteString, MimeType, HttpStatus)
readFileOrGetNotFoundHtml filePath = do
  -- TODO: Consider using something like https://github.com/bartavelle/filecache
  readResult <- liftIO
    ((try $ LB.readFile filePath) :: IO (Either IOException LB.ByteString))
  case readResult of
    Left ex -> do
      logException Warning ex
      liftIO
        $ return
            ( encodeUtf8 (ErrorResponses.fileNotFoundHtml filePath)
            , html
            , notFound
            )
    Right fileContent -> do
      contentType <- liftIO $ getContentType filePath
      return (fileContent, contentType, ok)
