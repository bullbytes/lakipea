module Http.Responses.Util where

import qualified Data.ByteString.Lazy          as BL
                                                ( length )


import           Data.List                      ( intercalate )
import           Data.Text.Lazy                 ( Text )
import           Data.Text.Lazy.Encoding        ( encodeUtf8 )
import qualified Data.ByteString.UTF8          as BSU
                                                ( toString )
import           Data.ByteString.Lazy.UTF8     as BLU
                                                ( fromString
                                                , ByteString
                                                )
import           Http.Constants
                                                ( lineBreak
                                                , doubleLineBreak
                                                , httpVersion
                                                , utf8
                                                )
import           Http.Status
import           Network.Mime

contentLengthHeader = "Content-Length: "
contentTypeHeader = "Content-Type: "

-- Creates an HTTP response from a body. The header contains
-- the mime type and the HTTP status
toResponse :: Text -> MimeType -> HttpStatus -> BLU.ByteString
toResponse body = toResponse_ (encodeUtf8 body)

contentLength :: (Show i, Integral i) => i -> String
contentLength length = contentLengthHeader <> show length

toResponse_ :: BLU.ByteString -> MimeType -> HttpStatus -> BLU.ByteString
toResponse_ body contentType status =
  -- To get the number of bytes in the response body, we need ByteString.Lazy's length.
  -- If we used ByteString.Lazy.UTF8's length, we'd get the number of characters which
  -- would lead to a "Content-Length" value in the response that's too short if the
  -- response contains characters that are encoded using more than one byte.
  let headerLines =
          [contentLength (BL.length body), contentTypeInUtf8 contentType]
  in  BLU.fromString (mkHeader status headerLines <> doubleLineBreak)
        <> body

contentTypeInUtf8 :: MimeType -> String
contentTypeInUtf8 contentType =
  contentTypeHeader <> BSU.toString contentType <> "; " <> utf8

-- Creates the header for a response, includes the Server header field
mkHeader :: HttpStatus -> [String] -> String
mkHeader status headerLines =
  statusLine
    <> lineBreak
    <> intercalate lineBreak headerLines
    <> lineBreak
    <> "Server: Lakipea"
  where statusLine = httpVersion <> " " <> show status
