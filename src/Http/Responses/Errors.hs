{-# LANGUAGE OverloadedStrings, QuasiQuotes #-}
module Http.Responses.Errors where

import           NeatInterpolation
import qualified Data.Text.Lazy                as LT
                                                ( Text
                                                , pack
                                                , fromStrict
                                                )
import qualified Data.Text                     as T
                                                ( pack )
import qualified Http.Status                   as Status

import           Data.ByteString.Lazy          as BL
                                                ( ByteString )
import qualified Http.ContentType              as ContentType
                                                ( plainText )
import           Http.Responses.Util            ( toResponse )

-- The client didn't send a host in the header → The request is invalid. See also:
-- https://serverfault.com/questions/894426/http-status-code-to-signal-bad-or-missing-host-header#894439
missingHost :: BL.ByteString
missingHost = badRequest "Request is missing a Host header"

-- A response with status "400 Bad Request".
badRequest :: LT.Text -> BL.ByteString
badRequest body = toResponse body ContentType.plainText Status.badRequest

forbidden :: FilePath -> BL.ByteString
forbidden requestedFile =
  let body =
          LT.pack $ "Accessing this file is not allowed: " <> show requestedFile
  in  toResponse body ContentType.plainText Status.forbidden

fileNotFoundHtml :: FilePath -> LT.Text
fileNotFoundHtml filePath =
  let filePathTxt = T.pack filePath
  in  LT.fromStrict [text|
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>File not found</title>
  </head>
  <body>
    <h2>Couldn't find file $filePathTxt</h2>
  </body>
</html>
|]
