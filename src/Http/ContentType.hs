{-# Language OverloadedStrings #-}
module Http.ContentType where

import           System.FilePath
import           System.Directory
import           Network.Mime
import           Data.Text                      ( pack )


plainText :: MimeType
plainText = "text/plain"

html :: MimeType
html = "text/html"

-- Gets the MIME type of the resource. Resolves symlinks
getContentType :: FilePath -> IO MimeType
getContentType resource = do
  filePath <- canonicalizePath resource
  let fileName = pack $ takeFileName filePath
  return $ mimeByExt defaultMimeMap defaultMimeType fileName
