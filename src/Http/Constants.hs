module Http.Constants where

httpVersion = "HTTP/1.1"
lineBreak = "\r\n"
doubleLineBreak = lineBreak ++ lineBreak
utf8 = "charset=UTF-8"
