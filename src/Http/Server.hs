-- A server speaking unencrypted HTTP.
{-# Language OverloadedStrings, ScopedTypeVariables #-}
module Http.Server
  ( handleConnection
  )
where
import           Network.Socket                 ( Socket )

import           Network.Socket.ByteString.Lazy ( recv
                                                , sendAll
                                                )
import           Control.Monad.IO.Class         ( liftIO )

import           Control.Monad.Reader           ( ask )

import qualified Data.ByteString.Lazy          as LB
                                                ( isSuffixOf
                                                , null
                                                , ByteString
                                                )

import           Http.Responses.Errors          ( missingHost )
import           Http.Responses.Files           ( acmeResponse )
import           Http.Responses.Redirects       ( redirectResponse )
import           Http.Constants                 ( doubleLineBreak )

import qualified Http.Requests                 as Requests

import           Data.List                      ( isPrefixOf )
import           LakipeaTypes

import           Data.ByteString.Lazy.UTF8     as BLU
                                                ( fromString )
readHeader :: Socket -> IO LB.ByteString
readHeader sock = go sock mempty
 where
  go sock prevContent = do
    newContent <- recv sock maxNumberOfBytesToReceive
    let content = prevContent <> newContent
    -- Read the data from the socket unless it's empty which means
    -- that the client has closed its socket. We also stop reading
    -- when the content ends with an empty line which marks the end of the HTTP header
    if LB.null newContent || emptyLine `LB.isSuffixOf` content
      then return content
      else go sock content
   where
    -- Only read one byte at a time to avoid reading further than the
    -- empty line separating the HTTP header from the HTTP body
    maxNumberOfBytesToReceive = 1
    emptyLine                 = BLU.fromString doubleLineBreak

-- Handles a connection by responding with a redirected (3xx). If
-- the request is a Let's Encrypt challenge, responds with the requested file
handleConnection :: Socket -> App ()
handleConnection connSock = do
  env <- ask
  let redirectPort = httpsRedirectPort $ config env
  clientHeader <- liftIO $ readHeader connSock
  let resource = Requests.requestedResource clientHeader
  response <- maybe
      -- No host in the client's header
    (return missingHost)
      -- Got a host → respond with the ACME file or a redirect to HTTPS
      -- The Let's Encrypt's certbot creates a file in a certain directory that
      -- we need to return via HTTP to fulfill the domain name challenge
    (\host -> if isAcmeChallenge resource
      then acmeResponse resource $ acmeChallengeWebroot $ config env
      else return (redirectResponse host redirectPort resource)
    )
      -- The client wants to get a resource from this host
    (Requests.host clientHeader)

  liftIO $ sendAll connSock response
 where
  isAcmeChallenge resource = "/.well-known/acme-challenge" `isPrefixOf` show resource
