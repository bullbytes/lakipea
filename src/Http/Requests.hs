{-# Language ScopedTypeVariables #-}
module Http.Requests where

import           Data.List                      ( isPrefixOf )

import qualified Data.ByteString.Lazy          as BL
                                                ( ByteString )
import qualified Data.ByteString.Lazy.UTF8     as BLU
                                                ( toString )
import           Safe                           ( atMay
                                                , headMay
                                                )
import           Data.List.Split                ( splitOn )
import           Utils.StringUtil               ( strip )
import           Utils.ListUtil                 ( splitAtFirst )
import           Text.Read                      ( readMaybe )
import           LakipeaTypes
import           Data.Maybe                     ( fromMaybe )
import qualified Data.Text                     as T
                                                ( pack )

import           Http.Constants
-- Gets the host name from the header of the client's request
host :: BL.ByteString -> Maybe HostName
host request = fmap fst (hostAndPort request)

-- Gets the host name and the port number from the
-- header of the client's request
hostAndPort :: BL.ByteString -> Maybe (HostName, Maybe Int)
hostAndPort header =
  let lines = splitOn lineBreak $ BLU.toString header
  in  fmap
        (\hostLine ->
          let hostAndPort        = strip ' ' $ drop (length hostPrefix) hostLine
              (hostStr, portStr) = splitAtFirst ':' hostAndPort
          in  (HostName hostStr, readMaybe portStr)
        )
        (headMay $ filter (isPrefixOf hostPrefix) lines)
  where hostPrefix = "Host:"

-- Get the resource the client requested from the header's request line
-- that looks like this "GET /index.html HTTP/1.1"
-- If there's no resource, return "/" as the default
requestedResource :: BL.ByteString -> Resource
requestedResource header =
  let lines         = splitOn lineBreak $ BLU.toString header
      resourceStrMb = do
        firstLine <- headMay lines
        let elements = words firstLine
        -- First comes the HTTP method, then the resource, then the HTTP version
        atMay elements 1
  in  Resource $ T.pack $ fromMaybe "/" resourceStrMb
