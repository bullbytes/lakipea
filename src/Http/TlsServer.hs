{-# Language OverloadedStrings, PatternSynonyms #-}
module Http.TlsServer
  ( handleConnection
  , loadCredentials
  )
where

import qualified Data.Text                     as T
                                                ( pack )
import qualified Network.TLS                   as TLS

import           Data.ByteString.UTF8          as BSU
                                                ( fromString )

import           Http.Responses.Files           ( fileResponse )
import           Http.Requests                 as Requests

import           Prelude                 hiding ( log )
import           Colog                          ( log
                                                , pattern I
                                                , pattern W
                                                , Severity(Warning)
                                                )
import           Http.Constants                 ( doubleLineBreak )
import           Utils.LogUtil                  ( logException )

import           LakipeaTypes

import           Control.Exception              ( try )
import           Data.Default.Class
import           Network.Socket


import qualified Network.TLS.Extra             as TE

import           Control.Monad.IO.Class         ( liftIO )
import           Http.Responses.Errors          ( missingHost )

import qualified Data.ByteString               as BS
import qualified Data.ByteString.Lazy          as BL
                                                ( fromStrict
                                                , ByteString
                                                )

handleConnection :: TLS.Credential -> Socket -> App ()
handleConnection tlsCred connSock = do
  context <- createTlsContext tlsCred connSock
  reply context
  liftIO $ TLS.contextClose context

logTlsInfo :: Either TLS.TLSException () -> TLS.Context -> App ()
logTlsInfo handShakeResult tlsContext = do
  -- Log the exception if there is one, do nothing otherwise
  either (logException Warning) return handShakeResult
  ctxInfoMb <- liftIO $ TLS.contextGetInformation tlsContext
  maybe
    (log W "Couldn't get TLS context info")
    (\info -> log I $ T.pack
      ("TLS version: " <> show (TLS.infoVersion info) <> ". Cipher: " <> show
        (TLS.infoCipher info)
      )
    )
    ctxInfoMb

createTlsContext :: TLS.Credential -> Socket -> App TLS.Context
createTlsContext tlsCred sock = do
  let creds = TLS.Credentials [tlsCred]
  ctx             <- TLS.contextNew sock (params creds)
  handShakeResult <- liftIO $ try $ TLS.handshake ctx
  logTlsInfo handShakeResult ctx
  return ctx
 where
  params :: TLS.Credentials -> TLS.ServerParams
  params creds = def
    { TLS.serverWantClientCert = False
    , TLS.serverShared = def { TLS.sharedCredentials = creds }
    -- See this for more cipher names: https://hackage.haskell.org/package/tls-1.5.2/docs/Network-TLS-Extra-Cipher.html
    , TLS.serverSupported = def { TLS.supportedVersions = [TLS.TLS12]
                                , TLS.supportedCiphers = [TE.cipher_AES256_SHA1]
                                }
    }

reply :: TLS.Context -> App ()
reply tlsContext = do
  clientMsg <- liftIO $ readMsg tlsContext
  let requestedResource = Requests.requestedResource clientMsg
  let hostMb            = Requests.host clientMsg
  -- TODO: Respond with a redirect or a resource
  response <- maybe (return missingHost) (fileResponse requestedResource) hostMb
  liftIO $ sendAndFlush tlsContext response
 where
  sendAndFlush :: TLS.Context -> BL.ByteString -> IO ()
  sendAndFlush tlsContext response = do
    -- https://hackage.haskell.org/package/tls-1.4.1/docs/Network-TLS.html#v:sendData
    TLS.sendData tlsContext response
    TLS.contextFlush tlsContext

readMsg :: TLS.Context -> IO BL.ByteString
readMsg context = go context mempty
 where
  go context prevContent = do
    -- Receive 16 KiB at a time, that's the TLS record size
    -- recvData returns a strict ByteString
    newContent <- TLS.recvData context
    let content = prevContent <> newContent
    -- Read the data from the socket unless it's empty which means
    -- that the client has closed its socket. We also stop reading
    -- when we see an empty line which marks the end of the HTTP header
    if BS.null newContent || emptyLine `BS.isSuffixOf` content
      then return $ BL.fromStrict content
      else go context content
    where emptyLine = BSU.fromString doubleLineBreak

-- https://hackage.haskell.org/package/tls-1.5.1/docs/Network-TLS.html#v:credentialLoadX509
loadCredentials :: FilePath -> FilePath -> IO (Either String TLS.Credential)
loadCredentials = TLS.credentialLoadX509
